<!doctype html>
<html lang="en">
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <title>Practica | HD Latinoamérica</title>

        <style>
            .banner{
                width: 100%;
                height: 45vh;

                background-image: url('/img/img-banner-{{ rand(1, 10) }}.jpg');
                background-attachment: fixed;
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
            }

            .box{
                width: 100%;
                height: 30vh;
            }

            form.card{
                border-radius: 0px 8px 0px 0px !important;
                -moz-border-radius: 0px 8px 0px 0px !important;
                -webkit-border-radius: 0px 8px 0px 0px !important;
            }

            @media only screen and (max-width: 768px) {

                form.card {
                    border-radius: 0px 0px 8px 8px !important;
                    -moz-border-radius: 0px 0px 8px 8px !important;
                    -webkit-border-radius: 0px 0px 8px 8px !important;
                }

            }
        </style>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>
<body>
    <div class="banner position-absolute"></div>
    <div class="container position-relative">
        <div class="box"></div>

        <div class="row w-100 m-0">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 p-0">
                <div class="card shadow rounded-0 p-3 border-light" style="border-radius: 8px 8px 0px 0px !important">
                    Crear licencia
                </div>
            </div>
        </div>
        <form action="{{ route('api.crear') }}" method="POST" class="card shadow p-3 border-light">
            @csrf
            <div class="row w-100 m-0">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 mt-2 mb-2">
                    <input type="text" class="form-control" id="Nombre" name="name" value="{{ old('name') }}" required placeholder="Nombre">
                    <div class="invalid-feedback">
                      Por favor, llena este campo
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 mt-2 mb-2">
                    <select class="custom-select" id="validationCustom04" required name="vig" value="{{ old('vig') }}" required>
                        <option selected disabled value="0">Vigencia</option>
                        <option value="1">5 minutos</option>
                        <option value="2">10 minutos</option>
                        <option value="3">15 minutos</option>
                        <option value="4">20 minutos</option>
                    </select>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 mt-2 mb-2">
                    <select class="custom-select" id="validationCustom04" required name="prod" value="{{ old('prod') }}" required>
                        <option selected disabled value="0">Producto</option>
                        <option value="1">5 equipos</option>
                        <option value="2">10 equipos</option>
                        <option value="3">15 equipos</option>
                        <option value="4">20 equipos</option>
                    </select>
                </div>
                <div class="col-12 col-sm-12 col-md-2 col-lg-2 mt-2 mb-2">
                    <button class="btn btn-primary w-100">Crear</button>
                </div>
            </div>
        </form>


        <h1 class="mt-5 mb-5">
            Licencias creadas
        </h1>

        <div class="table-responsive  mb-5">
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Codigo</th>
                        <th scope="col">Vigencia</th>
                        <th scope="col">Producto</th>
                        <th scope="col">Estatus</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($consulta as $key => $value)
                        <tr>
                            <th scope="row">{{ $value['id'] }}</th>
                            <td>{{ $value['nombre'] }}</td>
                            <td>{{ $value['codigo'] }}</td>
                            <td>{{ $value['vigencia'] }}</td>
                            <td>{{ $value['producto'] }} equipos</td>
                            <td>{{ $value['status'] }}</td>
                            <td>
                                @switch($value['accion'])
                                    @case(1)
                                        <form method="POST" action="{{ route('api.activar') }}">
                                            @csrf
                                            <input type="hidden" name="id_lic" value="{{ $value['id_lic'] }}">
                                            <button class="btn btn-success">
                                                Activar
                                            </button>
                                        </form>
                                        @break
                                    @case(2)
                                        <form method="POST" action="{{ route('api.desactivar') }}">
                                            @csrf
                                            <input type="hidden" name="id_lic" value="{{ $value['id_lic'] }}">
                                            <button class="btn btn-danger">
                                                Desactivar
                                            </button>
                                        </form>
                                        @break

                                    @case(3)
                                        <form method="POST" action="{{ route('api.renovar') }}">
                                            @csrf
                                            <input type="hidden" name="id_lic" value="{{ $value['id_lic'] }}">
                                            <button class="btn btn-info">
                                                Renovar
                                            </button>
                                        </form>
                                        @break

                                    @case(4)
                                        <form method="POST" action="{{ route('api.renovar') }}">
                                            @csrf
                                            <input type="hidden" name="id_lic" value="{{ $value['id_lic'] }}">
                                            <button class="btn btn-info">
                                                Renovar
                                            </button>
                                        </form>
                                        @break
                                    @default
                                @endswitch
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    @if (session()->exists('alert'))

        <script>
            Swal.fire({
                text: '{{ session()->get('alert')['mensaje'] }}',
                icon: '{{ session()->get('alert')['status'] }}',
                confirmButtonText: 'Continuar'
			})
			console.log('{{ session()->get('alert')['console'] }}');
        </script>

	@endif
</body>
</html>
